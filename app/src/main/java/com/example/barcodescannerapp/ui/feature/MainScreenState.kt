package com.example.barcodescannerapp.ui.feature

data class MainScreenState(
    val details:String = "Start scanning to get details"
)