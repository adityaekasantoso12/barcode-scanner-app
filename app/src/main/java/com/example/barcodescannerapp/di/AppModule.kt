package com.example.barcodescannerapp.di

import android.content.Context
import com.example.barcodescannerapp.data.repository.MainRepoImpl
import com.example.barcodescannerapp.data.ScanRepository
import com.example.barcodescannerapp.ui.feature.MainViewModel
import com.google.mlkit.vision.barcode.common.Barcode
import com.google.mlkit.vision.codescanner.GmsBarcodeScanner
import com.google.mlkit.vision.codescanner.GmsBarcodeScannerOptions
import com.google.mlkit.vision.codescanner.GmsBarcodeScanning
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {
    single<Context> { androidApplication().applicationContext }

    single<GmsBarcodeScannerOptions> {
        GmsBarcodeScannerOptions.Builder()
            .setBarcodeFormats(Barcode.FORMAT_ALL_FORMATS)
            .build()
    }
    single<GmsBarcodeScanner> {
        GmsBarcodeScanning.getClient(get(), get())
    }
    single<ScanRepository> { MainRepoImpl(get())
    }
    viewModel { MainViewModel(get()) }
}