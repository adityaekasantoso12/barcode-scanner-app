package com.example.barcodescannerapp.data

import kotlinx.coroutines.flow.Flow

interface ScanRepository {
    fun startScanning(): Flow<String?>
    suspend fun loginAndroidTV(qrCode: String)
}