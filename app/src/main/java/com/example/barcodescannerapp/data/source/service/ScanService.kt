package com.example.barcodescannerapp.data.source.service

import com.example.barcodescannerapp.data.source.response.ScanResponse
import retrofit2.http.POST
import retrofit2.http.Query

interface ScanService {
    @POST("users/v4/link-account/android-tv")
    suspend fun loginAndroidTV(
        @Query("qr_code") qrCode: String, @Query("userToken") userToken: String
    ): ScanResponse
}