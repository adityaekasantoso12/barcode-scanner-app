package com.example.barcodescannerapp.data.source.response

import kotlinx.serialization.Serializable

@Serializable
data class Scan(
    val qr_code: String,
    val userToken: String
)