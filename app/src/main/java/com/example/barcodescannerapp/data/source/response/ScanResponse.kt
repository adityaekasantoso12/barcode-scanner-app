package com.example.barcodescannerapp.data.source.response

import kotlinx.serialization.Serializable

@Serializable
data class ScanResponse(
    val scan: Scan
)